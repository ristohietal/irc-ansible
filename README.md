
    ansible-playbook playbook.yml -i production/hosts --vault-password-file vault_password.txt

ZNC ei lähde automaattisesti käyntiin, avaa se toiseen ikkunaan

    /usr/local/bin/znc -D
